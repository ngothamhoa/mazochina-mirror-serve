const OpenCC = require('opencc');
const LRUMap = require('lru_map').LRUMap;

const OCC_SC = new OpenCC('t2s.json');
const OCC_TC = new OpenCC('s2t.json');
const CONV_SC_CACHE = new LRUMap(1000);
const CONV_TC_CACHE = new LRUMap(1000);

module.exports.CONV_KEEP = text => text;
module.exports.CONV_SC = text => {
    let val = CONV_SC_CACHE.get(text);
    if (!val) {
        val = OCC_SC.convertSync(text);
        CONV_SC_CACHE.set(text, val);
    }
    return val;
};
module.exports.CONV_TC = text => {
    let val = CONV_TC_CACHE.get(text);
    if (!val) {
        val = OCC_TC.convertSync(text);
        CONV_TC_CACHE.set(text, val);
    }
    return val;
};