const escapeHTML = require('escape-html');

const findKeywords = (normalizedText: string, keywordsInScLowerCase: string[]): [number, number][] => {
    const keywordLocations: [number, number][] = []; // array of [start, end) pairs of highlight sections, sorted
    const search = (search: number, subIndex: 0 | 1): number => {
        let lower = -1;
        let upper = keywordLocations.length;
        while (upper - lower > 1) {
            const attempt = (lower + upper) >> 1;
            if (keywordLocations[attempt][subIndex] > search) {
                upper = attempt;
            } else {
                lower = attempt;
            }
        }
        return lower; // last one <= search.
    };
    const addKeyword = (start: number, end: number): void => {
        if (!keywordLocations.length) {
            keywordLocations.push([start, end]);
            return;
        }
        let lastOneToTheLeft = search(start, 1);
        if ((lastOneToTheLeft >= 0) && (keywordLocations[lastOneToTheLeft][1] === start)) { 
            lastOneToTheLeft -= 1;
        }
        if (lastOneToTheLeft + 1 < keywordLocations.length) {
            start = Math.min(start, keywordLocations[lastOneToTheLeft + 1][0]);
        }
        const lastOneToRemove = search(end, 0);
        if (lastOneToRemove >= 0) {
            end = Math.max(end, keywordLocations[lastOneToRemove][1]);
        }
        keywordLocations.splice(lastOneToTheLeft + 1, lastOneToRemove - lastOneToTheLeft, [start, end]);
    };
    for (const keyword of keywordsInScLowerCase) {
        let end = 0;
        while (true) {
            const matched = normalizedText.indexOf(keyword, end);
            end = matched + keyword.length;
            if (matched < 0) { break; }
            addKeyword(matched, end);
            for (const keyword of keywordsInScLowerCase) {
                let end = 0;
                while (true) {
                    const matched = normalizedText.indexOf(keyword, end);
                    end = matched + keyword.length;
                    if (matched < 0) { break; }
                    addKeyword(matched, end);
                }
            }
        }
    }
    return keywordLocations;
};

const highlight = (normalizedText: string, keywordsInScLowerCase: string[]): string => {
    const keywordLocations = findKeywords(normalizedText, keywordsInScLowerCase);
    let sb = '';
    let lastEnd = 0;
    for (const [start, end] of keywordLocations) {
        sb += `${escapeHTML(normalizedText.substring(lastEnd, start))}<mark>${escapeHTML(normalizedText.substring(start, end))}</mark>`;
        lastEnd = end;
    }
    sb += escapeHTML(normalizedText.substring(lastEnd));
    return sb;
};

module.exports.highlight = highlight;

const countSubstr = (haystack: string, needle: string): number => {
    let count = -1;
    for (let index = -0x7fffffff; index !== -1; count++, index = haystack.indexOf(needle, index + needle.length));
    return count;
};

const L25 = 25;

module.exports.excerpt = (normalizedText: string, keywordsInScLowerCase: string[]): string => {
    const lines = normalizedText.split('\n').map(line => line.trim()).filter(_ => _);
    const totalLength = lines.map(_ => _.length).reduce((a, b) => a + b, 0);
    if (totalLength <= 150) { return lines.map(line => highlight(line, keywordsInScLowerCase)).join('<br>'); }

    if (!keywordsInScLowerCase.length) {
        // Line selection: first three lines whose length >= min(50, median).
        const lineLengths = lines.map(line => line.length).sort();
        const medianLineLength = lineLengths.length ? lineLengths[(lineLengths.length - 1) >> 1] : 0;
        const lineLengthThreshold = Math.min(50, medianLineLength);
        let selectedLines = lines.filter(line => line.length > lineLengthThreshold);
        if (selectedLines.length < 3) { selectedLines = lines; }
        return `<p class="font-italic">（全文共 ${totalLength} 字符，预览如下）</p><ul class="list-group list-group-flush">` + selectedLines.slice(0, 3).map(line => `<li class="list-group-item">${escapeHTML(line.length > 75 ? line.substr(0, 75) + '…' : line)}</li>`).join('\n') + '</ul>';
    }

    const globalAppearancesByName = keywordsInScLowerCase.map(_ => 0);

    const excerpts: [number, string, number[], number][] = []; // each [index, string, appearences by keyword, total appearences]
    for (const line of lines) {
        const locs = findKeywords(line, keywordsInScLowerCase) as number[][];
        if (!locs.length) { continue; }
        const gaps = [];
        locs[0][2] = locs[0][1] - locs[0][0];
        for (let i = 1; i < locs.length; i++) {
            gaps.push([locs[i][0] - locs[i - 1][1], i]);
            locs[i][2] = locs[i][1] - locs[i][0];
        }
        gaps.sort((a, b) => a[0] - b[0]);
        for (const gap of gaps) {
            if (gap[0] > L25 * 2) { break; }
            let i = gap[1];
            // locs[i][2] is the block length if it's the LAST segment of the combined block, or -1
            const blockLengthBeforeGap = locs[i - 1][2];
            while (locs[i][2] === -1) { i++; }
            const blockLengthAfterGap = locs[i][2];
            const proposedNewSize = blockLengthBeforeGap + gap[0] + blockLengthAfterGap;
            if (proposedNewSize < 120) {
                locs[i][2] = proposedNewSize;
                locs[gap[1] - 1][2] = -1;
            }
        }
        // Merge sections that are < 50 characters apart, up until the length gets larger than 120.
        // Merging using a greedy approach that the smallest gap gets eliminated first.
        for (let i = 0; i < locs.length; i++) {
            if (locs[i][2] === -1) {
                continue;
            }
            const start = locs[i][1] - locs[i][2];
            const paddedStart = start - L25;
            const paddedEnd = locs[i][1] + L25;
            const outStr = ((paddedStart > 0) ? '…' : '') + line.substring(paddedStart, paddedEnd) + ((paddedEnd < line.length) ? '…' : '');
            const appearences = keywordsInScLowerCase.map(kw => countSubstr(outStr, kw));
            appearences.forEach((value, index) => globalAppearancesByName[index] += value);
            excerpts.push([excerpts.length, outStr, appearences, appearences.reduce((a, b) => a + b, 0)]);
        }
    }

    let selectedExcerpts: typeof excerpts = [];
    if (excerpts.length <= 3) { 
        selectedExcerpts = excerpts;
    } else {
        excerpts.sort((a, b) => b[3] - a[3]);
        selectedExcerpts.push(excerpts.shift()!);
        for (let i = 0; i < keywordsInScLowerCase.length; i++) {
            const selectedAppearences = selectedExcerpts.reduce((prev, curr) => prev + curr[2][i], 0);
            if (selectedAppearences <= 0) {
                const matchIndex = excerpts.findIndex(item => item[2][i] > 0);
                if (matchIndex >= 0) {
                    selectedExcerpts.push(excerpts.splice(matchIndex, 1)[0]);
                }
            }
        }
        while (selectedExcerpts.length < 3) {        
            selectedExcerpts.push(excerpts.shift()!);
        }
        selectedExcerpts.sort((a, b) => a[0] - b[0]);
    }

    const summary = globalAppearancesByName.map((count, i) => `「${keywordsInScLowerCase[i]}」${count} 次`);
    return `<p class="font-italic">（全文共出现${summary.join(', ')}，预览如下）</p><ul class="list-group list-group-flush">` + selectedExcerpts.map(item => `<li class="list-group-item">${highlight(item[1], keywordsInScLowerCase)}</li>`).join('\n') + '</ul>';
};