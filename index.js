const crypto = require('crypto');
const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const bodyParser = require('koa-bodyparser');
const pug = require('pug');
const options = require('./options');
const conf = require('./conf');
const toSc = require('./chinese').CONV_SC;
const sql = require('./db').query;
const { excerpt } = require('./highlight');

const parseDate = date => Math.floor(Date.parse(date.trim() + ' UTC') / 1000);

const cookieOptions = {
    maxAge: 2 * 365 * 86400 * 1000
};

const indexPage = pug.compileFile('index.pug');
const forumPage = pug.compileFile('forum.pug');
const threadPage = pug.compileFile('thread.pug');
const searchThreadPage = pug.compileFile('search_thread.pug');
const searchPostPage = pug.compileFile('search_post.pug');
const searchPage = pug.compileFile('search.pug');
const authorPage = pug.compileFile('author.pug');

const app = new Koa();
app.proxy = true;
app.use(bodyParser());

const router = new Router();

app.use(async (ctx, next) => {
    const config = options.currentConfiguration(ctx);
    ctx.userConfig = Object.assign(config, {
        render: options.base(config),
        date: timeStamp => {
            const str = new Date((timeStamp + parseInt(config.tz.substr(3)) * 3600) * 1000).toISOString();
            return str.substr(0, 10) + ' ' + str.substr(11, 5);
        }
    });
    await next();
});

router.get('/', async ctx => {
    const dbResults = (await sql('SELECT forum_targets.forum_id, forum_name, parent_forum_id, thread_count.num_of_threads, display_order, most_recent_update_include_children FROM forum_targets LEFT JOIN (SELECT forum_id, COUNT(thread_id) AS num_of_threads FROM thread_targets GROUP BY forum_id) AS thread_count ON forum_targets.forum_id = thread_count.forum_id ORDER BY forum_targets.display_order ASC')).rows;
    const filtered = dbResults.filter(row => row.parent_forum_id === 0);
    ctx.body = indexPage(Object.assign({
        forums: filtered.map((row, index) => ({
            hr: (index >= 1) && (row.display_order - filtered[index - 1].display_order > 10),
            count: row.num_of_threads,
            name: row.forum_name,
            link: '/forum/' + row.forum_id,
            childForums: dbResults.filter(srow => srow.parent_forum_id === row.forum_id).map(srow => ({
                name: srow.forum_name,
                link: '/forum/' + srow.forum_id
            })),
            lastPost: ctx.userConfig.date(row.most_recent_update_include_children)
        }))
    }, ctx.userConfig.render));
});

router.get('/forum/:id/:page?', async ctx => {
    const includeSpam = 'all' in ctx.query;

    const forumId = Math.floor(ctx.params.id);
    let page = Math.floor(ctx.params.page);
    if (isNaN(forumId)) { ctx.throw(400, 'Invalid forum ID.'); }
    if (isNaN(page) || page < 0) { page = 1; }
    const forum = (await sql('SELECT forum_name FROM forum_targets WHERE forum_id = $1', [forumId])).rows[0];
    if (!forum) { ctx.throw(404, 'No such forum.') }

    const threadIdRequest = sql('SELECT thread_id, thread_title, spam_likeness FROM thread_targets WHERE forum_id = $1 AND last_page_number > 0 AND successive_fail >= 0' + (includeSpam ? '' : ' AND spam_likeness < 35') + ' ORDER BY most_recent_update DESC LIMIT $2 OFFSET $3', [forumId, ctx.userConfig.fp,(page - 1) * ctx.userConfig.fp]);
    const countTotal = sql('SELECT COUNT(thread_id) AS thread_count FROM thread_targets WHERE forum_id = $1 AND last_page_number > 0 AND successive_fail >= 0' + (includeSpam ? '' : ' AND spam_likeness < 35'), [forumId]);
    const childForums = sql('SELECT forum_targets.forum_id, forum_name, thread_count.num_of_threads, most_recent_update_include_children FROM forum_targets LEFT JOIN (SELECT forum_id, COUNT(thread_id) AS num_of_threads FROM thread_targets GROUP BY forum_id) AS thread_count ON forum_targets.forum_id = thread_count.forum_id WHERE parent_forum_id = $1 ORDER BY display_order ASC', [forumId]);

    const threads = (await threadIdRequest).rows;
    if (!threads.length) {
        ctx.throw(400, 'Invalid page number.')
    }
    const skim = (await sql('SELECT meta.thread_id, pmin.user_name AS first_user, pmin.posted_date AS first_date, pmax.post_id AS last_id, pmax.user_name AS last_user, pmax.posted_date AS last_date, post_count FROM posts pmin RIGHT JOIN ((SELECT thread_id, COUNT(post_id) AS post_count, MIN(post_id) AS min_post, MAX(post_id) AS max_post FROM posts WHERE thread_id IN (' + threads.map(_ => _.thread_id).join(', ') + ') AND posts.most_recent_update >= 0 GROUP BY thread_id) AS meta LEFT JOIN posts pmax ON (pmax.post_id = meta.max_post)) ON (pmin.post_id = meta.min_post)')).rows;

    const total = (await countTotal).rows[0].thread_count;
    const maxPage = Math.ceil(total / ctx.userConfig.fp);

    ctx.body = forumPage(Object.assign({
        title: forum.forum_name,
        forumId,
        childForums: (await childForums).rows.map(row => ({
            count: row.num_of_threads,
            name: row.forum_name,
            link: '/forum/' + row.forum_id,
            lastPost: ctx.userConfig.date(row.most_recent_update_include_children)
        })),
        currentPage: page,
        linkExtra: ctx.querystring ? '?' + ctx.querystring : '',
        baseLink: '/forum/' + forumId,
        maxPage,
        postLink: 'http://forum.mazochina.com/posting.php?mode=post&f=' + forumId,
        includeSpam,
        threads: threads.map(row => {
            const skimRow = skim.filter(r => r.thread_id === row.thread_id)[0];
            if (!skimRow) { return null; }
            return {
                title: row.thread_title,
                firstLink: '/thread/' + row.thread_id,
                isSpam: row.spam_likeness >= 35,
                firstTime: ctx.userConfig.date(skimRow.first_date),
                firstUser: skimRow.first_user,
                lastLink: '/thread/' + row.thread_id + '/' + Math.ceil(skimRow.post_count / ctx.userConfig.tp) + '#p' + skimRow.last_id,
                lastTime: ctx.userConfig.date(skimRow.last_date),
                lastUser: skimRow.last_user,
                count: skimRow.post_count
            };
        }).filter(_ => _)
    }, ctx.userConfig.render));
});

router.get('/searchthread/:page?', async ctx => {
    let page = Math.floor(ctx.params.page);
    if (isNaN(page) || page < 0) { page = 1; }
    const keywords = (ctx.query.s || '').split(/\s+/).filter(_ => _);
    const username = (ctx.query.u || '').trim();
    if (!keywords.length && !username) {
        ctx.throw(400, 'No keyword or username is provided.');
    } else if (keywords.length && username) {
        ctx.throw(400, 'Cannot search on both criteria at the same time.');
    }
    if (keywords.length > 4) { ctx.throw(400, 'Too many keywords.'); }
    if (keywords.some(item => item.length > 20)) { ctx.throw(400, 'Keyword too long.'); }
    const normalizedKeywords = keywords.map(val => toSc(val.toLowerCase()));
    let forumId = null;
    let forumName = null;
    if ('f' in ctx.query) {
        forumId = Math.floor(ctx.query.f);
        if (isNaN(forumId)) { ctx.throw(400, 'Invalid forum ID.'); }
        const forum = (await sql('SELECT forum_name FROM forum_targets WHERE forum_id = $1', [forumId])).rows[0];
        if (!forum) { ctx.throw(404, 'No such forum.') }
        forumName = forum.forum_name;
    }

    let threadIdRequest;
    let countRequest;
    if (username) {
        const baseSqlCount = 'SELECT COUNT(thread_targets.thread_id) AS thread_count FROM posts p LEFT JOIN thread_targets ON (thread_targets.thread_id = p.thread_id) WHERE user_name = $1 AND NOT EXISTS (SELECT post_id FROM posts s WHERE s.thread_id = p.thread_id AND s.post_id < p.post_id) ' + (forumId ? ' AND forum_id = $2' : '') + ' AND last_page_number > 0 AND successive_fail >= 0';
        const baseSqlData = 'SELECT thread_targets.thread_id, thread_title FROM posts p LEFT JOIN thread_targets ON (thread_targets.thread_id = p.thread_id) WHERE user_name = $1 AND NOT EXISTS (SELECT post_id FROM posts s WHERE s.thread_id = p.thread_id AND s.post_id < p.post_id)' + (forumId ? ' AND forum_id = $4' : '') + ' AND last_page_number > 0 AND successive_fail >= 0 ORDER BY thread_id DESC LIMIT $2 OFFSET $3';
        countRequest = forumId ? sql(baseSqlCount, [username, forumId]) : sql(baseSqlCount, [username]);
        const params = [username, ctx.userConfig.fp,(page - 1) * ctx.userConfig.fp];
        threadIdRequest = forumId ? sql(baseSqlData, params.concat(forumId)) : sql(baseSqlData, params);
    } else {
        const likeClauses = normalizedKeywords.map((_, idx) => 'thread_title_sc LIKE $' + (idx + 1)).join(' AND ');
        const likeParams = normalizedKeywords.map(val => '%' + val.replace(/[%_]/g, '\\$&') + '%');
        const baseSql = 'SELECT thread_id, thread_title FROM thread_targets WHERE ' + likeClauses + (forumId ? ' AND forum_id = $' + (normalizedKeywords.length + 1) : '') + ' AND last_page_number > 0 AND successive_fail >= 0 ORDER BY most_recent_update DESC LIMIT 200';
        threadIdRequest = forumId ? sql(baseSql, likeParams.concat(forumId)) : sql(baseSql, likeParams);
    }
    const threads = (await threadIdRequest).rows;
    const skim = threads.length ? (await sql('SELECT meta.thread_id, pmin.user_name AS first_user, pmin.posted_date AS first_date, pmax.user_name AS last_user, pmax.posted_date AS last_date, post_count FROM posts pmin RIGHT JOIN ((SELECT thread_id, COUNT(post_id) AS post_count, MIN(post_id) AS min_post, MAX(post_id) AS max_post FROM posts WHERE thread_id IN (' + threads.map(_ => _.thread_id).join(', ') + ') AND posts.most_recent_update >= 0 GROUP BY thread_id) AS meta LEFT JOIN posts pmax ON (pmax.post_id = meta.max_post)) ON (pmin.post_id = meta.min_post)')).rows : [];

    let maxPage = 0;
    if (username) {
        const total = (await countRequest).rows[0].thread_count;
        maxPage = Math.ceil(total / ctx.userConfig.fp);
        if ((page > 1) && (page > maxPage)) {
            ctx.throw(400, 'Invalid page number.')
        }
    }

    ctx.body = searchThreadPage(Object.assign({
        forum: forumId ? {
            link: '/forum/' + forumId,
            name: forumName,
        } : null,
        username,
        keywords,
        normalizedKeywords,
        currentPage: page,
        linkExtra: '?' + ctx.querystring,
        baseLink: '/searchthread',
        maxPage,
        threads: threads.map(row => {
            const skimRow = skim.filter(r => r.thread_id === row.thread_id)[0];
            return {
                title: row.thread_title,
                firstLink: '/thread/' + row.thread_id,
                firstTime: ctx.userConfig.date(skimRow.first_date),
                firstUser: skimRow.first_user,
                lastLink: '/thread/' + row.thread_id + '/' + Math.ceil(skimRow.post_count / ctx.userConfig.tp),
                lastTime: ctx.userConfig.date(skimRow.last_date),
                lastUser: skimRow.last_user,
                count: skimRow.post_count
            };
        })
    }, ctx.userConfig.render));
});

router.post('/author/:id/blacklist', async ctx => {
    const key = ctx.cookies.get(conf.COOKIE_PREFIX + 'admin_key'); // Key m4A*********WG4
    const hash = crypto.createHash('sha256');
    hash.update(key + '-a-bad-salt');
    if (hash.digest('hex') !== '8ae23e705de6e6c5345a7464756cb25e9bff902b5fbb328b707bcb3cb1150e50') {
        ctx.throw(403, 'Bad admin key.');
    }
    const userId = Math.floor(ctx.params.id);
    if (isNaN(userId)) { ctx.throw(400, 'Invalid author ID.'); }
    const user = (await sql('SELECT user_name, marked_spam_at FROM users WHERE user_id = $1', [userId])).rows[0];
    if (!user) {
        ctx.throw(404, 'User not exists.');
    }
    if (user.marked_spam_at) {
        ctx.throw(412, 'User already blacklisted.');
    }
    const threadIdRequest = await sql('SELECT thread_targets.thread_id, thread_title FROM posts p LEFT JOIN thread_targets ON (thread_targets.thread_id = p.thread_id) WHERE user_name = $1 AND NOT EXISTS (SELECT post_id FROM posts s WHERE s.thread_id = p.thread_id AND s.post_id < p.post_id)', [user.user_name]);
    const threadIds = threadIdRequest.rows.map(_ => _.thread_id);
    await sql('UPDATE thread_targets SET spam_likeness = spam_likeness + 35 WHERE thread_id IN (' + threadIds.join(', ') + ')');
    await sql('UPDATE users SET marked_spam_at = $1 WHERE user_id = $2', [Math.floor(Date.now() / 1000), userId]);
    ctx.body = 'Blocked threads: ' + JSON.stringify(threadIds);
});

router.get('/author/:id', async ctx => {
    const userId = Math.floor(ctx.params.id);
    if (isNaN(userId)) { ctx.throw(400, 'Invalid author ID.'); }
    const userMeta = (await sql('SELECT user_name, registered_at, marked_spam_at FROM users WHERE user_id = $1', [userId])).rows[0];
    if (!userMeta) { ctx.throw(404, 'No such author.') }
    const userName = userMeta.user_name;
    const threadIdRequest = sql('SELECT thread_targets.thread_id, thread_title FROM posts p LEFT JOIN thread_targets ON (thread_targets.thread_id = p.thread_id) WHERE user_name = $1 AND NOT EXISTS (SELECT post_id FROM posts s WHERE s.thread_id = p.thread_id AND s.post_id < p.post_id) AND last_page_number > 0 AND successive_fail >= 0 ORDER BY thread_id DESC LIMIT 10', [userName]);
    const sqlThreadCount = sql('SELECT COUNT(thread_targets.thread_id) AS thread_count FROM posts p LEFT JOIN thread_targets ON (thread_targets.thread_id = p.thread_id) WHERE user_name = $1 AND NOT EXISTS (SELECT post_id FROM posts s WHERE s.thread_id = p.thread_id AND s.post_id < p.post_id) AND last_page_number > 0 AND successive_fail >= 0', [userName]);
    const sqlPostCount = sql('SELECT COUNT(post_id) AS post_count FROM posts p LEFT JOIN thread_targets ON (thread_targets.thread_id = p.thread_id) WHERE user_name = $1 AND p.most_recent_update >= 0 AND successive_fail >= 0', [userName]);
    const threads = (await threadIdRequest).rows;
    const skim = threads.length ? (await sql('SELECT meta.thread_id, pmin.user_name AS first_user, pmin.posted_date AS first_date, pmax.user_name AS last_user, pmax.posted_date AS last_date, post_count FROM posts pmin RIGHT JOIN ((SELECT thread_id, COUNT(post_id) AS post_count, MIN(post_id) AS min_post, MAX(post_id) AS max_post FROM posts WHERE thread_id IN (' + threads.map(_ => _.thread_id).join(', ') + ') AND posts.most_recent_update >= 0 GROUP BY thread_id) AS meta LEFT JOIN posts pmax ON (pmax.post_id = meta.max_post)) ON (pmin.post_id = meta.min_post)')).rows : [];
    ctx.body = authorPage(Object.assign({
        userName,
        userId,
        registeredAt: ctx.userConfig.date(userMeta.registered_at),
        blacklisted: !!userMeta.marked_spam_at,
        isAdmin: !!ctx.cookies.get(conf.COOKIE_PREFIX + 'admin_key'),
        threadCount: (await sqlThreadCount).rows[0].thread_count,
        postCount: (await sqlPostCount).rows[0].post_count,
        threads: threads.map(row => {
            const skimRow = skim.filter(r => r.thread_id === row.thread_id)[0];
            return {
                title: row.thread_title,
                firstLink: '/thread/' + row.thread_id,
                firstTime: ctx.userConfig.date(skimRow.first_date),
                firstUser: skimRow.first_user,
                lastLink: '/thread/' + row.thread_id + '/' + Math.ceil(skimRow.post_count / ctx.userConfig.tp),
                lastTime: ctx.userConfig.date(skimRow.last_date),
                lastUser: skimRow.last_user,
                count: skimRow.post_count
            };
        })
    }, ctx.userConfig.render));
});

const processContent = (content, dateFormatter) => {
    return content.replace(/<img class="smilies" src=".\/images\/smilies\/icon_/g, '<img class="smilies" src="/static/smilies/icon_').replace(/<blockquote>/g, '<blockquote class="card p-3">').replace(/<cite><a href="\.\/memberlist\.php\?mode=viewprofile&amp;u=(\d+)">([^<>]+)<\/a> wrote: <a href="\.\/viewtopic\.php\?p=\d+#p\d+" data-post-id="(\d+)" onclick="if\(document\.getElementById\(hash\.substr\(1\)\)\)href=hash">↑<\/a><div class="responsive-hide">([^<>]+)<\/div><\/cite>/g, (str, userId, userName, postId, date) => '<cite>' + userName + '：<a href="javascript:G0P05t(' + postId + ')">↑</a><span class="text-muted float-right d-none d-sm-inline">' + dateFormatter(parseDate(date)) + '</span></cite>');
};

const processContentSc = (content, dateFormatter) => {
    const separatorIndex = content.indexOf('\ufffc');
    return (separatorIndex >= 0) ? content.substr(separatorIndex + 1) : processContent(content, dateFormatter);
};

router.get('/searchpost/:page?', async ctx => {
    let page = Math.floor(ctx.params.page);
    if (isNaN(page) || page < 0) { page = 1; }
    const keywords = (ctx.query.s || '').split(/\s+/).filter(_ => _);
    const username = (ctx.query.u || '').trim();
    if (!keywords.length && !username) {
        ctx.throw(400, 'No keyword or username is provided.');
    } else if (keywords.length && username) {
        ctx.throw(400, 'Cannot search on both criteria at the same time.');
    }
    if (keywords.length > 4) { ctx.throw(400, 'Too many keywords.'); }
    if (keywords.some(item => item.length > 20)) { ctx.throw(400, 'Keyword too long.'); }
    const normalizedKeywords = keywords.map(val => toSc(val.toLowerCase()));
    let forumId = null;
    let forumName = null;
    if ('f' in ctx.query) {
        forumId = Math.floor(ctx.query.f);
        if (isNaN(forumId)) { ctx.throw(400, 'Invalid forum ID.'); }
        const forum = (await sql('SELECT forum_name FROM forum_targets WHERE forum_id = $1', [forumId])).rows[0];
        if (!forum) { ctx.throw(404, 'No such forum.') }
        forumName = forum.forum_name;
    }

    let postsRequest;
    let countRequest;
    let userMetaRequest;
    if (username) {
        const baseSqlCount = 'SELECT COUNT(post_id) AS post_count FROM posts p LEFT JOIN thread_targets ON (thread_targets.thread_id = p.thread_id) WHERE user_name = $1' + (forumId ? ' AND forum_id = $2' : '') + ' AND p.most_recent_update >= 0 AND successive_fail >= 0';
        const baseSqlData = 'SELECT post_id, title, content_sc, posted_date, posts.thread_id, forum_id FROM posts LEFT JOIN thread_targets ON (thread_targets.thread_id = posts.thread_id) WHERE user_name = $1' + (forumId ? ' AND forum_id = $4' : '') + ' AND posts.most_recent_update >= 0 AND successive_fail >= 0 ORDER BY post_id DESC LIMIT $2 OFFSET $3';
        countRequest = forumId ? sql(baseSqlCount, [username, forumId]) : sql(baseSqlCount, [username]);
        const params = [username, ctx.userConfig.tp,(page - 1) * ctx.userConfig.tp];
        postsRequest = forumId ? sql(baseSqlData, params.concat(forumId)) : sql(baseSqlData, params);
        userMetaRequest = sql('SELECT user_id FROM users WHERE user_name = $1', [username]);
    } else {
        const likeClauses = normalizedKeywords.map((_, idx) => 'content_sc LIKE $' + (idx + 1)).join(' AND ');
        const likeParams = normalizedKeywords.map(val => '%' + val.replace(/[%_]/g, '\\$&') + '%');
        const baseSql = 'SELECT post_id, title, posts.user_name, content_sc, posted_date, posts.thread_id, user_id, forum_id FROM posts LEFT JOIN thread_targets ON (thread_targets.thread_id = posts.thread_id) LEFT JOIN users ON (posts.user_name = users.user_name) WHERE ' + likeClauses + (forumId ? ' AND forum_id = $' + (normalizedKeywords.length + 1) : '') + ' AND successive_fail >= 0 ORDER BY post_id DESC LIMIT 100';
        postsRequest = forumId ? sql(baseSql, likeParams.concat(forumId)) : sql(baseSql, likeParams);
    }
    const posts = (await postsRequest).rows;

    let maxPage = 0;
    if (username) {
        const total = (await countRequest).rows[0].post_count;
        maxPage = Math.ceil(total / ctx.userConfig.tp);
        const userMeta = (await userMetaRequest).rows[0];
        posts.forEach(row => {
           row.user_name = username;
           row.user_id = userMeta ? userMeta.user_id : null;
        });
        if ((page > 1) && (page > maxPage)) {
            ctx.throw(400, 'Invalid page number.')
        }
    }

    ctx.body = searchPostPage(Object.assign({
        forum: forumId ? {
            link: '/forum/' + forumId,
            name: forumName,
        } : null,
        username,
        keywords,
        normalizedKeywords,
        currentPage: page,
        linkExtra: '?' + ctx.querystring,
        baseLink: '/searchpost',
        maxPage,
        excerpt,
        posts: posts.map(row => ({
            authorLink: row.user_id ? '/author/' + row.user_id : null,
            authorName: row.user_name,
            id: row.post_id,
            title: row.title,
            content: processContentSc(row.content_sc, ctx.userConfig.date),
            date: ctx.userConfig.date(row.posted_date),
            replyLink: 'http://forum.mazochina.com/posting.php?mode=quote&f=' + row.forum_id + '&p=' + row.post_id,
            topicLink: '/post/' + row.post_id,
        }))
    }, ctx.userConfig.render));
});

router.get('/post/:postId', async ctx => {
    const postId = Math.floor(ctx.params.postId);
    if (isNaN(postId)) { ctx.throw(400, 'Invalid post ID.'); }

    const meta = (await sql('SELECT thread_id, most_recent_update FROM posts WHERE post_id = $1', [postId])).rows[0];
    if (!meta) { ctx.throw(404, 'No such post.'); }
    if (meta.most_recent_update < 0) { ctx.throw(404, 'Post has been deleted.'); } 
    const index = (await sql('WITH summary AS (SELECT post_id, ROW_NUMBER() OVER (ORDER BY post_id) AS position FROM posts WHERE thread_id = $1) SELECT position FROM summary WHERE post_id = $2', [meta.thread_id, postId])).rows[0];

    const pageNumber = Math.floor((index.position - 1) / ctx.userConfig.tp) + 1;
    ctx.redirect('/thread/' + meta.thread_id + '/' + pageNumber + '#p' + postId);
});

router.post('/invalidate/thread/:id/:page?', async ctx => {
    const threadId = Math.floor(ctx.params.id);
    const meta = (await sql('SELECT last_success_crawl, successive_fail, last_full_scan_requested FROM thread_targets WHERE thread_id = $1', [threadId])).rows[0];

    if (!meta) { ctx.throw(404, 'No such thread.'); }

    if (meta.successive_fail < 0) {
        ctx.body = 'Thread already deleted.';
        return;
    }

    if (meta.last_success_crawl === 1) {
        ctx.body = 'Cache invalidation already queued.';
        return;
    }

    const effectiveLastUpdateTime = (meta.last_full_scan_requested !== 1) ? meta.last_full_scan_requested : meta.last_success_crawl;
    const daysToWait = 3;
    const timeToWait = (daysToWait * 86400) - (Math.floor(Date.now() / 1000) - effectiveLastUpdateTime);
    if (timeToWait > 0) {
        ctx.body = `Last cache invalidation was made within ${daysToWait} days. Wait for ${Math.ceil(timeToWait / 60)} more minutes before retrying.`;
        return;
    }

    console.log(`Cache invalidation for thread ${threadId} from ${ctx.ip}: previous timestamps were ${meta.last_success_crawl}/${meta.last_full_scan_requested}`);
    await sql('UPDATE thread_targets SET last_success_crawl = 1, last_full_scan_requested = 1 WHERE thread_id = $1', [threadId]);
    ctx.body = `Cache invalidation queued for ${threadId}. It may take a few minutes (under certain rare circumstances, a few hours) before the content actually gets refreshed.`;
});

router.get('/thread/:id/:page?', async ctx => {
    const threadId = Math.floor(ctx.params.id);
    let page = Math.floor(ctx.params.page);
    if (isNaN(threadId)) { ctx.throw(400, 'Invalid thread ID.'); }
    if (isNaN(page) || page < 0) { page = 1; }
    const thread = (await sql('SELECT forum_id, thread_title, poll_title FROM thread_targets WHERE thread_id = $1', [threadId])).rows[0];
    if (!thread) { ctx.throw(404, 'No such thread.'); }
    if (isNaN(page) || page < 0) { page = 1; }

    let postsRequest, authorOnly, countTotal;
    const author = ctx.query.author;
    if (author && author.length) {
        postsRequest = sql('SELECT post_id, title, posts.user_name, content, posted_date, user_id FROM posts LEFT JOIN users ON (posts.user_name = users.user_name) WHERE thread_id = $1 AND posts.user_name = $2 AND posts.most_recent_update >= 0 ORDER BY post_id ASC LIMIT $3 OFFSET $4', [threadId, author, ctx.userConfig.tp, (page - 1) * ctx.userConfig.tp]);
        authorOnly = author;
        countTotal = sql('SELECT COUNT(post_id) as post_count FROM posts WHERE thread_id = $1 AND user_name = $2 AND most_recent_update >= 0', [threadId, author]);
    } else {
        postsRequest = sql('SELECT post_id, title, posts.user_name, content, posted_date, user_id FROM posts LEFT JOIN users ON (posts.user_name = users.user_name) WHERE thread_id = $1 AND most_recent_update >= 0 ORDER BY post_id ASC LIMIT $2 OFFSET $3', [threadId, ctx.userConfig.tp, (page - 1) * ctx.userConfig.tp]);
        authorOnly = null;
        countTotal = sql('SELECT COUNT(post_id) as post_count FROM posts WHERE thread_id = $1 AND posts.most_recent_update >= 0', [threadId]);
    }

    const parentForum = sql('SELECT forum_name FROM forum_targets WHERE forum_id = $1', [thread.forum_id]);
    const posts = (await postsRequest).rows;
    if (!posts.length) {
        ctx.throw(400, 'Invalid page number.')
    }

    const total = (await countTotal).rows[0].post_count;
    const maxPage = Math.ceil(total / ctx.userConfig.tp);

    ctx.body = threadPage(Object.assign({
        title: thread.thread_title,
        poll: thread.poll_title || null,
        authorOnly,
        currentPage: page,
        linkExtra: ctx.querystring ? '?' + ctx.querystring : '',
        baseLink: '/thread/' + threadId,
        maxPage,
        replyLink: 'http://forum.mazochina.com/posting.php?mode=reply&f=' + thread.forum_id + '&t=' + threadId,
        originalLink: 'http://forum.mazochina.com/viewtopic.php?f=' + thread.forum_id + '&t=' + threadId,
        parent: {
            name: (await parentForum).rows[0].forum_name,
            link: '/forum/' + thread.forum_id
        },
        posts: posts.map(row => ({
            authorLink: row.user_id ? '/author/' + row.user_id : null,
            authorName: row.user_name,
            id: row.post_id,
            title: row.title,
            content: processContent(row.content, ctx.userConfig.date),
            date: ctx.userConfig.date(row.posted_date),
            replyLink: 'http://forum.mazochina.com/posting.php?mode=quote&f=' + thread.forum_id + '&p=' + row.post_id,
            originalLink: 'http://forum.mazochina.com/viewtopic.php?f=' + thread.forum_id + '&t=' + threadId + '&p=' + row.post_id + '#p' + row.post_id,
            authorOnlyLink: '/thread/' + threadId + '?author=' + encodeURIComponent(row.user_name)
        }))
    }, ctx.userConfig.render));
});

router.get('/search', async ctx => {
    let forumId;
    if ('f' in ctx.query) {
        forumId = Math.floor(ctx.query.f);
        if (isNaN(forumId)) { ctx.throw(400, 'Invalid forum ID.'); }
    }
    const dbResults = (await sql('SELECT forum_id, forum_name, parent_forum_id FROM forum_targets ORDER BY forum_targets.display_order ASC')).rows;
    const output = [];
    const recursiveAdd = (parentId, prefix) => dbResults.filter(forum => forum.parent_forum_id === parentId).map(row => {
        const prefixStr = prefix + '　　';
        output.push({ id: row.forum_id, name: prefixStr + row.forum_name });
        recursiveAdd(row.forum_id, prefixStr);
    });
    recursiveAdd(0, '');
    ctx.body = searchPage(Object.assign({
        forums: output,
        forumId
    }, ctx.userConfig.render));
});

router.post('/configure', async ctx => {
    ['theme', 'tz', 'fp', 'tp', 'cc'].forEach(key => {
        ctx.cookies.set(conf.COOKIE_PREFIX + key, ctx.request.body[key], cookieOptions);
    });
    ctx.redirect('back', '/');
});

app.use(serve('.'));
app.use(router.routes());

app.listen(process.env['mirror_serve_port'] || 3000, '127.0.0.1');
