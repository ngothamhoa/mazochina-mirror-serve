const COOKIE_PREFIX = require('./conf').COOKIE_PREFIX;
const { CONV_KEEP, CONV_TC, CONV_SC } = require('./chinese');
const { highlight } = require('./highlight');

const themes = {
    'default': 'https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
    'cosmo': 'https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/cosmo/bootstrap.min.css',
    'litera': 'https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/litera/bootstrap.min.css',
    'lux': 'https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/lux/bootstrap.min.css',
    'materia': 'https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/materia/bootstrap.min.css',
    'minty': 'https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/minty/bootstrap.min.css',
    'sketchy': 'https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/sketchy/bootstrap.min.css',
    'slate': 'https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/slate/bootstrap.min.css',
    'solar': 'https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/solar/bootstrap.min.css',
    'spacelab': 'https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/spacelab/bootstrap.min.css',
    'united': 'https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/united/bootstrap.min.css',
    'yeti': 'https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/yeti/bootstrap.min.css'
};

const tzs = [];
for (let i = -12; i <= 12; i++) {
    tzs.push('UTC' + (i > 0 ? '+' : '') + i);
}

const forumPc = [20, 30, 50, 100];
const threadPc = [10, 20, 30, 50];

const ccOptions = {
    'scnc': '简体界面，内容不作转换',
    'sccc': '简体界面，内容转为简体',
    'tcnc': '繁体界面，内容不作转换',
    'tccc': '繁体界面，内容转为繁体'
};

module.exports.currentConfiguration = ctx => {
    let theme = ctx.cookies.get(COOKIE_PREFIX + 'theme');
    let tz = ctx.cookies.get(COOKIE_PREFIX + 'tz');
    let fp = parseInt(ctx.cookies.get(COOKIE_PREFIX + 'fp'));
    let tp = parseInt(ctx.cookies.get(COOKIE_PREFIX + 'tp'));
    let cc = ctx.cookies.get(COOKIE_PREFIX + 'cc');
    if (!(theme in themes)) {
        theme = 'united';
    }
    if (!tzs.includes(tz)) {
        tz = 'UTC+8';
    }
    if (!forumPc.includes(fp)) {
        fp = 30;
    }
    if (!threadPc.includes(tp)) {
        tp = 20;
    }
    if (!(cc in ccOptions)) {
        cc = 'scnc';
    }
    return { tz, theme, fp, tp, cc };
};

module.exports.base = config => {
    const { tz, theme, fp, tp, cc } = config;
    const contentConv = (cc === 'sccc') ? CONV_SC : ((cc === 'tccc') ? CONV_TC : CONV_KEEP);
    return {
        footer: {
            selectedTz: tz, tzs,
            selectedTheme: theme, themes: Object.keys(themes),
            selectedForumPc: fp, forumPc,
            selectedThreadPc: tp, threadPc,
            selectedCc: cc, ccs: ccOptions
        },
        themeCss: themes[theme],
        staticConv: cc.startsWith('tc') ? CONV_TC : CONV_KEEP,
        forceConv: cc.startsWith('tc') ? CONV_TC : CONV_SC,
        contentConv,
        highlight: (text, keywordsInScLowerCase) => contentConv(highlight(CONV_SC(text.toLowerCase()), keywordsInScLowerCase))
    };
};