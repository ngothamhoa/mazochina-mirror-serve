var escapeHTML = require('escape-html');
var findKeywords = function (normalizedText, keywordsInScLowerCase) {
    var keywordLocations = []; // array of [start, end) pairs of highlight sections, sorted
    var search = function (search, subIndex) {
        var lower = -1;
        var upper = keywordLocations.length;
        while (upper - lower > 1) {
            var attempt = (lower + upper) >> 1;
            if (keywordLocations[attempt][subIndex] > search) {
                upper = attempt;
            }
            else {
                lower = attempt;
            }
        }
        return lower; // last one <= search.
    };
    var addKeyword = function (start, end) {
        if (!keywordLocations.length) {
            keywordLocations.push([start, end]);
            return;
        }
        var lastOneToTheLeft = search(start, 1);
        if ((lastOneToTheLeft >= 0) && (keywordLocations[lastOneToTheLeft][1] === start)) {
            lastOneToTheLeft -= 1;
        }
        if (lastOneToTheLeft + 1 < keywordLocations.length) {
            start = Math.min(start, keywordLocations[lastOneToTheLeft + 1][0]);
        }
        var lastOneToRemove = search(end, 0);
        if (lastOneToRemove >= 0) {
            end = Math.max(end, keywordLocations[lastOneToRemove][1]);
        }
        keywordLocations.splice(lastOneToTheLeft + 1, lastOneToRemove - lastOneToTheLeft, [start, end]);
    };
    for (var _i = 0, keywordsInScLowerCase_1 = keywordsInScLowerCase; _i < keywordsInScLowerCase_1.length; _i++) {
        var keyword = keywordsInScLowerCase_1[_i];
        var end = 0;
        while (true) {
            var matched = normalizedText.indexOf(keyword, end);
            end = matched + keyword.length;
            if (matched < 0) {
                break;
            }
            addKeyword(matched, end);
            for (var _a = 0, keywordsInScLowerCase_2 = keywordsInScLowerCase; _a < keywordsInScLowerCase_2.length; _a++) {
                var keyword_1 = keywordsInScLowerCase_2[_a];
                var end_1 = 0;
                while (true) {
                    var matched_1 = normalizedText.indexOf(keyword_1, end_1);
                    end_1 = matched_1 + keyword_1.length;
                    if (matched_1 < 0) {
                        break;
                    }
                    addKeyword(matched_1, end_1);
                }
            }
        }
    }
    return keywordLocations;
};
var highlight = function (normalizedText, keywordsInScLowerCase) {
    var keywordLocations = findKeywords(normalizedText, keywordsInScLowerCase);
    var sb = '';
    var lastEnd = 0;
    for (var _i = 0, keywordLocations_1 = keywordLocations; _i < keywordLocations_1.length; _i++) {
        var _a = keywordLocations_1[_i], start = _a[0], end = _a[1];
        sb += escapeHTML(normalizedText.substring(lastEnd, start)) + "<mark>" + escapeHTML(normalizedText.substring(start, end)) + "</mark>";
        lastEnd = end;
    }
    sb += escapeHTML(normalizedText.substring(lastEnd));
    return sb;
};
module.exports.highlight = highlight;
var countSubstr = function (haystack, needle) {
    var count = -1;
    for (var index = -0x7fffffff; index !== -1; count++, index = haystack.indexOf(needle, index + needle.length))
        ;
    return count;
};
var L25 = 25;
module.exports.excerpt = function (normalizedText, keywordsInScLowerCase) {
    var lines = normalizedText.split('\n').map(function (line) { return line.trim(); }).filter(function (_) { return _; });
    var totalLength = lines.map(function (_) { return _.length; }).reduce(function (a, b) { return a + b; }, 0);
    if (totalLength <= 150) {
        return lines.map(function (line) { return highlight(line, keywordsInScLowerCase); }).join('<br>');
    }
    if (!keywordsInScLowerCase.length) {
        return "<p class=\"font-italic\">\uFF08\u5168\u6587\u5171 " + totalLength + " \u5B57\u7B26\uFF0C\u9884\u89C8\u5982\u4E0B\uFF09</p><ul class=\"list-group list-group-flush\">" + lines.slice(0, 3).map(function (line) { return "<li class=\"list-group-item\">" + escapeHTML(line.length > 75 ? line.substr(0, 75) + '…' : line) + "</li>"; }).join('\n') + '</ul>';
    }
    var globalAppearancesByName = keywordsInScLowerCase.map(function (_) { return 0; });
    var excerpts = []; // each [index, string, appearences by keyword, total appearences]
    for (var _i = 0, lines_1 = lines; _i < lines_1.length; _i++) {
        var line = lines_1[_i];
        var locs = findKeywords(line, keywordsInScLowerCase);
        if (!locs.length) {
            continue;
        }
        var gaps = [];
        locs[0][2] = locs[0][1] - locs[0][0];
        for (var i = 1; i < locs.length; i++) {
            gaps.push([locs[i][0] - locs[i - 1][1], i]);
            locs[i][2] = locs[i][1] - locs[i][0];
        }
        gaps.sort(function (a, b) { return a[0] - b[0]; });
        for (var _a = 0, gaps_1 = gaps; _a < gaps_1.length; _a++) {
            var gap = gaps_1[_a];
            if (gap[0] > L25 * 2) {
                break;
            }
            var i = gap[1];
            // locs[i][2] is the block length if it's the LAST segment of the combined block, or -1
            var blockLengthBeforeGap = locs[i - 1][2];
            while (locs[i][2] === -1) {
                i++;
            }
            var blockLengthAfterGap = locs[i][2];
            var proposedNewSize = blockLengthBeforeGap + gap[0] + blockLengthAfterGap;
            if (proposedNewSize < 120) {
                locs[i][2] = proposedNewSize;
                locs[gap[1] - 1][2] = -1;
            }
        }
        var _loop_1 = function (i) {
            if (locs[i][2] === -1) {
                return "continue";
            }
            var start = locs[i][1] - locs[i][2];
            var paddedStart = start - L25;
            var paddedEnd = locs[i][1] + L25;
            var outStr = ((paddedStart > 0) ? '…' : '') + line.substring(paddedStart, paddedEnd) + ((paddedEnd < line.length) ? '…' : '');
            var appearences = keywordsInScLowerCase.map(function (kw) { return countSubstr(outStr, kw); });
            appearences.forEach(function (value, index) { return globalAppearancesByName[index] += value; });
            excerpts.push([excerpts.length, outStr, appearences, appearences.reduce(function (a, b) { return a + b; }, 0)]);
        };
        // Merge sections that are < 50 characters apart, up until the length gets larger than 120.
        // Merging using a greedy approach that the smallest gap gets eliminated first.
        for (var i = 0; i < locs.length; i++) {
            _loop_1(i);
        }
    }
    var selectedExcerpts = [];
    if (excerpts.length <= 3) {
        selectedExcerpts = excerpts;
    }
    else {
        excerpts.sort(function (a, b) { return b[3] - a[3]; });
        selectedExcerpts.push(excerpts.shift());
        var _loop_2 = function (i) {
            var selectedAppearences = selectedExcerpts.reduce(function (prev, curr) { return prev + curr[2][i]; }, 0);
            if (selectedAppearences <= 0) {
                var matchIndex = excerpts.findIndex(function (item) { return item[2][i] > 0; });
                if (matchIndex >= 0) {
                    selectedExcerpts.push(excerpts.splice(matchIndex, 1)[0]);
                }
            }
        };
        for (var i = 0; i < keywordsInScLowerCase.length; i++) {
            _loop_2(i);
        }
        while (selectedExcerpts.length < 3) {
            selectedExcerpts.push(excerpts.shift());
        }
        selectedExcerpts.sort(function (a, b) { return a[0] - b[0]; });
    }
    var summary = globalAppearancesByName.map(function (count, i) { return "\u300C" + keywordsInScLowerCase[i] + "\u300D" + count + " \u6B21"; });
    return "<p class=\"font-italic\">\uFF08\u5168\u6587\u5171\u51FA\u73B0" + summary.join(', ') + "\uFF0C\u9884\u89C8\u5982\u4E0B\uFF09</p><ul class=\"list-group list-group-flush\">" + selectedExcerpts.map(function (item) { return "<li class=\"list-group-item\">" + highlight(item[1], keywordsInScLowerCase) + "</li>"; }).join('\n') + '</ul>';
};
